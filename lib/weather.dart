import 'package:flutter/material.dart';

void main() {
  runApp(WeatherApp());
}

class WeatherApp extends StatefulWidget {
  @override
  State<WeatherApp> createState() => _WeatherAppState();
}

class _WeatherAppState extends State<WeatherApp> {
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.black,
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.black,
    leading: Icon(
      Icons.list,
      color: Colors.white,
    ),
    actions: <Widget>[
      IconButton(
          onPressed: () {},
          icon: Icon(Icons.add_circle_outline),
          color: Colors.white),
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            child: Text(
              "Amphoe Mueang Chon Buri",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 22,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            child: Text(
              "27°C",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 80,
                color: Colors.blue,
              ),
            ),
          ),
          Container(
            child: Text(
              "Mostly Sunny",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(5),
            margin: EdgeInsets.only(bottom: 5),
            child: Text(
              "H:29°C  L: 22°C",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
              ),
            ),
          ),

          //list temp today
          Container(
            height: 170,
            margin: EdgeInsets.fromLTRB(20, 1, 20, 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.blue,
            ),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(15),
                  child: Text(
                      'Party cloudy conditions expected around 6PM.',
                      style: TextStyle(
                          fontSize: 14, color: Colors.white, fontWeight: FontWeight.bold)
                  ),
                ),
                Divider(
                  color: Colors.white,
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      TempToday1(),
                      TempToday2(),
                      TempToday3(),
                      TempToday4(),
                      TempToday5(),
                      TempToday6(),
                    ],
                  ),
                )
              ],
            ),
          ),

          //7-day forecast
          Container(
            height: 500,
            margin: EdgeInsets.fromLTRB(20, 1, 20, 70),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.grey,
            ),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.all(15),
                  child: Text(
                      '7-DAYS FORECAST',
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,fontWeight: FontWeight.bold)
                  ),
                ),
                Divider(
                  color: Colors.white,
                  height: 1,
                ),
                Container(
                  padding: EdgeInsets.all(1),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Today(),

                      Monday(),
                      Tueday(),
                      Wednesday(),
                      Thursday(),
                      Friday(),
                      Satday(),
                      Sunday(),
                    ],
                  ),
                )
              ],
            ),
          ),

        ],
      )
    ],
  );
}

//list temp today
Widget TempToday1(){
  return Column(
    children: <Widget>[
      Text("Now",style: TextStyle(color: Colors.white)
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("27°C",style: TextStyle(color: Colors.white)
      ),
    ],
  );
}

Widget TempToday2(){
  return Column(
    children: <Widget>[
      Text("12PM",style: TextStyle(color: Colors.white)
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("27°C",style: TextStyle(color: Colors.white)
      ),

    ],
  );
}

Widget TempToday3(){
  return Column(
    children: <Widget>[
      Text("1PM",style: TextStyle(color: Colors.white)
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("28°C",style: TextStyle(color: Colors.white)
      ),
    ],
  );
}

Widget TempToday4(){
  return Column(
    children: <Widget>[
      Text("2PM",style: TextStyle(color: Colors.white)
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("28°C",style: TextStyle(color: Colors.white)
      ),

    ],
  );
}

Widget TempToday5(){
  return Column(
    children: <Widget>[
      Text("3PM",style: TextStyle(color: Colors.white)
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("28°C",style: TextStyle(color: Colors.white)
      ),
    ],
  );
}

Widget TempToday6(){
  return Column(
    children: <Widget>[
      Text("4PM",style: TextStyle(color: Colors.white)
      ),
      IconButton(
        icon: Icon(
          Icons.sunny,
          color: Colors.yellow,
        ),
        onPressed: () {},
      ),
      Text("29°C",style: TextStyle(color: Colors.white)
      ),
    ],
  );
}

//Forecast
Widget Today() {
  return ListTile(
    leading: Text("Today",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),
    title: Icon(Icons.sunny ,color: Colors.yellow),
    trailing: Text("     20°C             30°C",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),

  );
}

Widget Monday() {
  return ListTile(
    leading: Text("Mon",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),
    title: Icon(Icons.sunny ,color: Colors.yellow),
    trailing: Text("     20°C             30°C",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),

  );
}

Widget Tueday() {
  return ListTile(
    leading: Text("Tue",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),
    title: Icon(Icons.sunny ,color: Colors.yellow),
    trailing: Text("     20°C             30°C",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),

  );
}

Widget Wednesday() {
  return ListTile(
    leading: Text("Wed",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),
    title: Icon(Icons.sunny ,color: Colors.yellow),
    trailing: Text("     20°C             30°C",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),

  );
}

Widget Thursday() {
  return ListTile(
    leading: Text("Thu",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),
    title: Icon(Icons.sunny ,color: Colors.yellow),
    trailing: Text("     20°C             30°C",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),

  );
}

Widget Friday() {
  return ListTile(
    leading: Text("Fri",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),
    title: Icon(Icons.sunny ,color: Colors.yellow),
    trailing: Text("     20°C             30°C",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),

  );
}

Widget Satday() {
  return ListTile(
    leading: Text("Sat",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),
    title: Icon(Icons.sunny ,color:
    Colors.yellow),
    trailing: Text("     20°C             30°C",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),

  );
}

Widget Sunday() {
  return ListTile(
    leading: Text("Sun",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),
    title: Icon(Icons.sunny ,color: Colors.yellow),
    trailing: Text("     20°C             30°C",
        style: TextStyle(color: Colors.white,fontSize: 18)
    ),

  );
}



